The script takes as input a path to a video file,
separates audio from video, performs a short time Fourier transform on the audio signal and plots the corresponding spectrogram for each video frame. Returns the same video with the spectrogram of the audio signal.




Usage:
create_video.py --video_path "path to your video"



Resulting video:
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/5PRDG1x5Vdk/0.jpg)](https://www.youtube.com/watch?v=5PRDG1x5Vdk)



