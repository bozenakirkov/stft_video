import shutil
import datetime
import argparse
import os
import sys
import subprocess

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import get_window
import cv2

from audio_transformations import stft_analysis, read_wave

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../models/'))


def create_one_frame(x=None, fs=None, idx=None, tmst=None, window='hamming', M=1024, N=1024, H=512):
    """
    Analyze input signal using STFT.
    :param x: input sound (monophonic with sampling rate of 44100)
    :param fs: sampling rate
    :param idx: index of video frame
    :param tmst: timestamp
    :param window: window type
    :param M: analysis window size
    :param N: fft size (power of two, bigger or equal than M)
    :param H: hop size (at least 1/2 of analysis window size to have good overlap-add)
    :return: spectrogram of a given input sound
    """
    w = get_window(window, M)

    # compute the magnitude spectrogram
    mX = stft_analysis(x, w, N, H)

    plt.figure(num=1, figsize=(4, 4))

    # ranges to plot
    max_plot_freq = 1000
    min_plot_db = -120

    num_frames = int(mX[:, 0].size)
    frames_time = H * np.arange(num_frames) / float(fs)
    bin_freq = fs * np.arange(N * max_plot_freq / fs) / N

    title = "Spectrogram"
    ax = plt.axes(projection='3d')
    ax.view_init(azim=20, elev=30)
    ax.set_title(title, loc='center')

    x_time = frames_time[None, :]
    y_frequency = bin_freq[:, None]
    z_amplitude_db = np.transpose(mX[:, :int(N * max_plot_freq / fs + 1)])
    ax.plot_surface(x_time, y_frequency, z_amplitude_db, cmap='viridis')
    ax.set_xlabel(f'timespan ({x.size/fs} s)')
    ax.set_ylabel('frequencies (Hz)')
    ax.set_zlabel('amplitude (dB)')
    ax.set_zlim(min_plot_db, 0)
    ax.set_box_aspect(None, zoom=0.9)

    ax.set_xticks(ticks=[0, 0.05, 0.10, 0.15, 0.20, 0.25], labels=[])

    output_folder = "outputs"
    plt.savefig(output_folder + f"/spectrograms/{tmst}_{idx}.jpeg", transparent=True)
    plt.close(fig=1)


def prepare_workspace(clean_workspace=True):
    """
    Create directories to store images, audio, and video.
    :param clean_workspace:
    :return:
    """
    if clean_workspace:
        try:
            shutil.rmtree('audio')
            shutil.rmtree('video')
            shutil.rmtree('outputs')
        except FileNotFoundError as e:
            print(e)
        os.mkdir('audio')
        os.mkdir('video')
        os.makedirs('outputs/spectrograms')
        os.makedirs('outputs/waves')


def normalize_audio(signal):
    """
    Normalize the signal:
        - Calculate the scaling ratio to map the signal to [-1, 1],
        - Shift the signal to have a midpoint of 0
    :param signal:
    :return:
    """
    int16_fac = (2 ** 15)
    int32_fac = (2 ** 31)
    int64_fac = (2 ** 63)
    norm_fact = {
        'int16': int16_fac,
        'int32': int32_fac,
        'int64': int64_fac,
        'float32': 1.0,
        'float64': 1.0
    }
    normalized_signal = np.float32(signal) / norm_fact[signal.dtype.name]
    # ratio = 2 / (np.max(signal) - np.min(signal))
    # shift = (np.max(signal) + np.min(signal)) / 2
    # normalized_signal = ratio * (signal - shift)
    # normalized_signal = 2 * ((signal - np.min(signal)) /
    #                          (np.max(signal) - np.min(signal))) - 1
    return normalized_signal


def calculate_spec_plot_size(img_width, img_height):
    """
    Calculates size of the spectrogram plot
    based on the input video resolution.
    :param img_width:
    :param img_height:
    :return:
    """
    img_to_plot_ratio = 0.3

    if img_height/img_width == 720/1280:
        img_to_plot_ratio = 0.25
    elif img_height/img_width == 480/640:
        img_to_plot_ratio = 0.3

    square_plot_size = int(img_to_plot_ratio * img_width)
    return square_plot_size


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--video_path", help="a path to the video file", required=True, type=str)
    args = parser.parse_args()

    prepare_workspace()

    timestamp = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')

    input_video = args.video_path
    input_wave = f"audio/{timestamp}_soundfile.wav"

    subprocess.run(["ffmpeg", "-i", input_video,
                    "-vn", "-ac", "1", input_wave],
                   shell=True)

    fs, x_full_original = read_wave(input_wave)
    x_full = normalize_audio(x_full_original)

    videocap = cv2.VideoCapture(input_video)
    width = int(videocap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(videocap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    frame_rate = videocap.get(cv2.CAP_PROP_FPS)

    hop_size = int(fs / frame_rate)

    for i in range(int(((len(x_full) - int(fs / 4)) * frame_rate) / fs)):
        x = x_full[hop_size * i:hop_size * i + int(fs / 4)]
        create_one_frame(x=x, fs=fs, idx=i, tmst=timestamp)

    temp_path = f"video/{timestamp}_temp_clip.avi"
    inputs = "outputs"

    output = cv2.VideoWriter(
        temp_path, cv2.VideoWriter_fourcc(*"XVID"),
        frame_rate, (width, height)
    )

    square_size = calculate_spec_plot_size(width, height)

    for img_num in range(len(os.listdir(inputs + "/spectrograms"))):
        success, image = videocap.read()
        if not success:
            break
        frame = cv2.imread(os.path.join(inputs, f"spectrograms/{timestamp}_{img_num}.jpeg"))
        frame = cv2.resize(frame, (square_size, square_size))
        image[0:square_size, -square_size:] = frame
        output.write(image)

    cv2.destroyAllWindows()
    output.release()

    final_video = f"video/{timestamp}_final_video.avi"

    subprocess.run(["ffmpeg", "-i", temp_path, "-i", input_wave, "-c", "copy", "-map", "0:v:0",
                    "-map", "1:a:0", final_video])

    os.remove(temp_path)
