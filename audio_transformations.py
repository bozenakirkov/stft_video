import os
import numpy as np
from scipy.fft import fft
from scipy.io.wavfile import read


def read_wave(audio_file_path):
    """
    Read a sound file and convert it to a normalized floating point array
    :param audio_file_path:
    :return: sampling rate of file, x: floating point array
    """
    if not os.path.isfile(audio_file_path):
        raise ValueError("Input file not found")

    fs, x = read(audio_file_path)

    if len(x.shape) != 1:
        raise ValueError("Audio file should be mono")

    return fs, x


def stft_analysis(x, w, N, H):
    """
    Analysis of a sound using the short-time Fourier transform.
    :param x: input array sound
    :param w: analysis window
    :param N: FFT size
    :param H: hop size
    :return: magnitude spectra
    """
    if H <= 0:  # raise error if hop size 0 or negative
        raise ValueError("Hop size (H) smaller or equal to 0")

    M = w.size  # size of analysis window
    hM1 = (M + 1) // 2  # half analysis window size by rounding
    hM2 = M // 2  # half analysis window size by floor
    x = np.append(np.zeros(hM2), x)  # add zeros at beginning to center first window at sample 0
    x = np.append(x, np.zeros(hM2))  # add zeros at the end to analyze last sample
    pin = hM1  # initialize sound pointer in middle of analysis window
    pend = x.size - hM1  # last sample to start a frame
    w = w / sum(w)  # normalize analysis window
    xmX = []  # Initialise empty list for mX

    while pin <= pend:  # while sound pointer is smaller than last sample
        x1 = x[pin - hM1:pin + hM2]  # select one frame of input sound
        mX = dft_analysis(x1, w, N)  # compute dft
        xmX.append(np.array(mX))  # Append output to list
        pin += H  # advance sound pointer
    xmX = np.array(xmX)  # Convert to numpy array
    return xmX


def dft_analysis(x, w, N):
    """
    Analysis of a signal using the discrete Fourier transform
    :param x: input signal
    :param w: analysis window
    :param N: FFT size
    :return: magnitude spectrum
    """
    # Check if N is power of two
    if not ((N & (N - 1)) == 0) and N > 0:  # raise error if N not a power of two
        raise ValueError("FFT size (N) is not a power of 2")

    if w.size > N:  # raise error if window size bigger than fft size
        raise ValueError("Window size (M) is bigger than FFT size")

    hN = (N // 2) + 1  # size of positive spectrum, it includes sample 0
    hM1 = (w.size + 1) // 2  # half analysis window size by rounding
    hM2 = w.size // 2  # half analysis window size by floor
    fftbuffer = np.zeros(N)  # initialize buffer for FFT
    w = w / sum(w)  # normalize analysis window
    xw = x * w  # window the input sound
    fftbuffer[:hM1] = xw[hM2:]  # zero-phase window in fftbuffer
    fftbuffer[-hM2:] = xw[:hM2]
    X = fft(fftbuffer)  # compute FFT
    absX = abs(X[:hN])  # compute absolute value of positive side
    absX[absX < np.finfo(float).eps] = np.finfo(float).eps  # if zeros add epsilon to handle log
    mX = 20 * np.log10(absX)  # magnitude spectrum of positive frequencies in dB
    return mX
